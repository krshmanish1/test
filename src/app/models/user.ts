export interface User {
    avatar_url: string;
    html_url: string;
    id: number;
    login: string;
    repos_url: string;
    type: string;
}