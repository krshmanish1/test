import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  private _users: User[] = [];
  constructor(private httpClient: HttpClient) { }

  saveUsers(users: User[]) {
    this._users = users;
  }

  fetchUsers() {
    return this._users;
  }

  clearUsers() {
    this._users = [];
  }

  getUsers() {
    return this.httpClient.get('https://api.github.com/users');
  }

  getUserById(id: number) {
    return this.httpClient.get('https://api.github.com/user/' + id);
  }

  getAllUsers() {
    return this.httpClient.get('https://api.github.com/users');
  }
}
