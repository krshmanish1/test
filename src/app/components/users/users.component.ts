import { Component, OnInit } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { User } from 'src/app/models/user';
import { Router } from '@angular/router';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {

  users: User[];
  isLoading: boolean;

  constructor(private userService: UsersService, private route: Router) { }

  ngOnInit() {
    this.isLoading = false;
    this.users = this.userService.fetchUsers();
    if (!this.users.length) {
      this.isLoading = true;
      this.userService.getUsers().subscribe((res: User[]) => {
        this.users = res;
        this.userService.saveUsers(this.users);
        this.isLoading = false;
      }, error => {
        console.log('Error : ', error);
        this.isLoading = false;
      });
    }
  }

  onSeeDetails(user: User) {
    this.route.navigate(['user', user.id]);
  }
}
