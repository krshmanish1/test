import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserComponent } from '../user/user.component';
import {
  MatProgressBarModule,
  MatProgressSpinnerModule,
  MatCardModule
} from '@angular/material';
import { RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';

describe('UserComponent', () => {
  let component: UserComponent;
  let fixture: ComponentFixture<UserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [UserComponent],
      imports: [
        MatProgressBarModule,
        MatProgressSpinnerModule,
        MatCardModule,
        RouterModule.forRoot([]),
        HttpClientModule
      ]
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
