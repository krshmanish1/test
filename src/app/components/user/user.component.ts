import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { User } from 'src/app/models/user';
import { UsersService } from 'src/app/services/users.service';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.less']
})
export class UserComponent implements OnInit {

  user: User;
  isLoading: boolean;
  isEven: boolean;
  isOdd: boolean;

  constructor(private activatedRoute: ActivatedRoute,
    private usersService: UsersService) { }

  ngOnInit() {
    this.activatedRoute.params.subscribe(params => {
      if (params) {
        this.getUserById(params.user_id);
      }
    });
  }

  getUserById(id: number) {
    this.isLoading = true;
    this.usersService.getUserById(id).subscribe((user: User) => {
      this.user = user;
      this.isEven = this.isOdd = false;
      if (this.user.id % 2 === 0) {
        this.isEven = true;
      } else {
        this.isOdd = true;
      }
      this.isLoading = false;
    }, error => {
      console.log('Error : ', error);
      this.isLoading = false;
    });
  }

  onClick() {
    this.isEven = this.isOdd = false;
    if (this.user.id % 2 === 0) {
      this.isEven = true;
    } else {
      this.isOdd = true;
    }
  }
}
